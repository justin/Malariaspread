#ifndef METEO_HPP
#define METEO_HPP

#include <fstream>

#include <vle/devs/Dynamics.hpp>

class Meteo : public vle::devs::Dynamics
{
public:
    Meteo(const vle::graph::AtomicModel& model,
          const vle::devs::InitEventList& lst);

    virtual ~Meteo();

    virtual vle::devs::Time timeAdvance() const;

    virtual vle::devs::Time init(const vle::devs::Time& time);

    virtual void internalTransition(const vle::devs::Time& time);

    virtual void output(const vle::devs::Time& currentTime,
                        vle::devs::ExternalEventList& output) const;

    virtual vle::value::Value* observation(const vle::devs::ObservationEvent& event) const;

    virtual void updateState();

private:

    enum State { STATE1, STATE2 };

    // Phase du mod�le
    State state;

    // Working variables
    std::ifstream file;
    std::string line;

    // Parameters
    std::string file_path;

    //! Variables d'�tat du mod�le
    int year;
    int month;
    int day;
    double Tmin;
    double Tmax;
    double Tmoy;
    double Hmin;
    double Hmax;
    double Hmoy;
};

DECLARE_DYNAMICS(Meteo);

#endif
