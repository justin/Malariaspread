/*
 * @file malaria/Si.cpp
 *
 * This file is part of VLE, a framework for multi-modeling, simulation
 * and analysis of complex dynamical systems
 * http://www.vle-project.org
 *
 * Copyright (c) 2011 INRA http://www.inra.fr
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* @@tagdepends: vle.extension.differential-equation @@endtagdepends
   @@tagdynamic@@ */

#include <vle/extension/DifferentialEquation.hpp>

#include <iostream>
#include <sstream>

namespace malariaspread  {

    namespace ved = vle::extension::differential_equation;

    class E21h : 
         public ved::DifferentialEquation
    {
    public:
        E21h(const vle::devs::DynamicsInit& model,
           const vle::devs::InitEventList& events) :
            ved::DifferentialEquation(model,events)
        {
            
            k_1  = events.getDouble("k_1"); // = ???
            
	    beta_1  = events.getDouble("beta_1"); // = ???
            
            delta_H1  = events.getDouble("delta_H1"); // = ???
            
            d1_H1  = events.getDouble("d1_H1"); // = ???

            H_1  = events.getDouble("H_1"); // = ???

            phi_e2  = events.getDouble("phi_e2"); // = ???
       
           
            
            _E21h = createVar("E21h");
            _S21h = createExt("S21h");
            _I1v = createExt("I1v");
            //_Pth1 = createExt("Pth1");
            _E2h= createExt("E2h");

          /*_S1v= createExt("S1v");
            _E1v= createExt("E1v"); */

            
        }

        virtual ~E21h()
        { }

        void compute(const vle::devs::Time& /* time */)
        {
                        
            grad(_E21h) =  phi_e2*_E2h()  + k_1*beta_1*_S21h()*_I1v()/H_1 - delta_H1*_E21h() - d1_H1*_E21h();
        }

    private:
        
        //contact proportion between a human belonging to the patch 1 and a mosquitoe belonging to the patch 1
        double beta_1;
        //average number of that contact per unit time for that patch 1
        double k_1;
        // transition rate from E -> I for the patch 1 of the host population
        double delta_H1;
        // natural death rate of E in the host population for the patch 1
        double d1_H1;        

        // size of host population for the patch 1
        double H_1;

          // migration rate of Infected (E) of host population of the patch 2 that left to patch 1 (Go)
        double phi_e2;        
       
        double migr;
               
        
        Var _E21h;
        Ext _S21h;
        Ext _I1v;
        //Ext _Pth1;
        Ext _E2h;

      /* Ext _S1v;
         Ext _E1v; */
        
    };

} // namespace malariaspread

DECLARE_DYNAMICS(malariaspread::E21h)

 