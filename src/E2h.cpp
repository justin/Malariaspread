/*
 * @file malaria/Si.cpp
 *
 * This file is part of VLE, a framework for multi-modeling, simulation
 * and analysis of complex dynamical systems
 * http://www.vle-project.org
 *
 * Copyright (c) 2011 INRA http://www.inra.fr
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* @@tagdepends: vle.extension.differential-equation @@endtagdepends
   @@tagdynamic@@ */

#include <vle/extension/DifferentialEquation.hpp>

#include <iostream>
#include <sstream>

namespace malariaspread  {

    namespace ved = vle::extension::differential_equation;

    class E2h : 
         public ved::DifferentialEquation
    {
    public:
        E2h(const vle::devs::DynamicsInit& model,
           const vle::devs::InitEventList& events) :
            ved::DifferentialEquation(model,events)
        {
            
            //k_2  = events.getDouble("k_2"); // = ???
            
	    //beta_2  = events.getDouble("beta_2"); // = ???

            b_2  = events.getDouble("b_2"); // = ???
            
            delta_H2  = events.getDouble("delta_H2"); // = ???
            
            d2_H2  = events.getDouble("d2_H2"); // = ???

            H_2  = events.getDouble("H_2"); // = ???

            phi_e2  = events.getDouble("phi_e2"); // = ???
       
            phi_e1  = events.getDouble("phi_e1"); // = ???
            
            _E2h = createVar("E2h");
            _S2h = createExt("S2h");
            _I2v = createExt("I2v");
            //_Pth2 = createExt("Pth2");
            _E1h= createExt("E1h");

          /*_S2v= createExt("S2v");
            _E2v= createExt("E2v"); */

            
        }

        virtual ~E2h()
        { }

        void compute(const vle::devs::Time& /* time */)
        {
            // V_2 = grad(_S2v) + grad(_E2v) + grad(_I2v);   //current size of all compartments at time t  

            grad(_E2h) = phi_e1*_E1h() - phi_e2*_E2h() + b_2*_S2h()*_I2v()/H_2 - delta_H2*_E2h() - d2_H2*_E2h();
        }

    private:
        
        //contact proportion between a human belonging to the patch 2 and a mosquitoe belonging to the patch 2
        //double beta_2;
        //average number of that contact per unit time for that patch 2
        //double k_2;
        //inoculation rate of patch 2
        double b_2;
        // transition rate from E -> I for the patch 1 of the host population
        double delta_H2;
        // natural death rate of E in the host population for the patch 2
        double d2_H2;        

        // size of host population for the patch 2
        double H_2;

        // migration rate of Infected (E) of host population of the patch 2 that left to patch 1 (Go)
        double phi_e2;        
         // migration rate of Infected (E) of host population of the patch 1 that come to patch 2 (Come)
        double phi_e1;  
               
        
        Var _E2h;
        Ext _S2h;
        Ext _I2v;
        //Ext _Pth2;
        Ext _E1h;

      /* Ext _S2v;
         Ext _E2v; */
       
        
    };

} // namespace malariaspread

DECLARE_DYNAMICS(malariaspread::E2h)

 