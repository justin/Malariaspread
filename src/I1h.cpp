/*
 * @file malaria/Si.cpp
 *
 * This file is part of VLE, a framework for multi-modeling, simulation
 * and analysis of complex dynamical systems
 * http://www.vle-project.org
 *
 * Copyright (c) 2011 INRA http://www.inra.fr
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* @@tagdepends: vle.extension.differential-equation @@endtagdepends
   @@tagdynamic@@ */

#include <vle/extension/DifferentialEquation.hpp>

#include <iostream>
#include <sstream>

namespace malariaspread  {

    namespace ved = vle::extension::differential_equation;

    class I1h :
        public ved::DifferentialEquation
    {
    public:
        I1h(const vle::devs::DynamicsInit& model,
           const vle::devs::InitEventList& events) :
            ved::DifferentialEquation(model,events)
        {
            delta_H1  = events.getDouble("delta_H1");; // = ???
            
	    alpha_1  = events.getDouble("alpha_1"); // = ???
           
            gamma_1  = events.getDouble("gamma_1"); // = ???
            
            d1_H1  = events.getDouble("d1_H1"); // = ???

                   
            _I1h = createVar("I1h");
            _E1h = createExt("E1h");           

            
        }

        virtual ~I1h()
        { }

        void compute(const vle::devs::Time& /* time */)
        {
            grad(_I1h) = delta_H1*_E1h() -alpha_1*_I1h() - gamma_1*_I1h() - d1_H1*_I1h();
        }

    private:
        
        // transition rate from E -> I for the patch 1 of the host population
        double delta_H1;
        // transition rate from I -> R for the patch 1 of the host population
        double alpha_1;
        //death rate due to the disease
        double gamma_1;
        // natural death rate of I in the host population for the patch 1
        double d1_H1;        
                       
        Var _I1h;
        Ext _E1h;
                
    };

} // namespace malariaspread

DECLARE_DYNAMICS(malariaspread::I1h)

 