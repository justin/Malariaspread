/*
 * @file malaria/Si.cpp
 *
 * This file is part of VLE, a framework for multi-modeling, simulation
 * and analysis of complex dynamical systems
 * http://www.vle-project.org
 *
 * Copyright (c) 2011 INRA http://www.inra.fr
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* @@tagdepends: vle.extension.differential-equation @@endtagdepends
   @@tagdynamic@@ */

#include <vle/extension/DifferentialEquation.hpp>

#include <iostream>
#include <sstream>

namespace malariaspread  {

    namespace ved = vle::extension::differential_equation;

    class I1v :
        public ved::DifferentialEquation
    {
    public:
        I1v(const vle::devs::DynamicsInit& model,
           const vle::devs::InitEventList& events) :
            ved::DifferentialEquation(model,events)
        {
            
            
            delta_V1  = events.getDouble("delta_V1");; // = ???

            d1_V1  = events.getDouble("d1_V1"); // = ???
                  
                     
            _I1v = createVar("I1v");
            _E1v = createExt("E1v"); 
            
            _T1 = createExt("Tmin");
            _Rh1 = createExt("Hmax");       

            
        }

        virtual ~I1v()
        { }

        void compute(const vle::devs::Time& /* time */)
        {
            beta0 = 0.00113*_Rh1()*_Rh1()-0.158*_Rh1()-6.61; 
            beta1 = -2.32*pow(10,-4)*_Rh1()*_Rh1()+0.0515*_Rh1()+1.06;
            beta2 = 4*pow(10,-6)*_Rh1()*_Rh1()-1.09*pow(10,-3)*_Rh1()-0.0255;          
            
            ptrh1 = -log(exp(-1/(beta0+beta1*_T1()+beta2*_T1()*_T1())));    // mortality rate of the vector at temperature t1 and relative humidity rh1
             
             
          // grad(_I1v) = delta_V1*_E1v() - ptrh1*_I1v() - d1_V1*_I1v();
         grad(_I1v) = delta_V1*_E1v() - ptrh1*_I1v();

        }

    private:
       
        // transition rate from E -> I for the patch 1 of the vector population
        double delta_V1;
        // natural death rate of I of vector population for the patch 1
        double d1_V1;   
          //coefficients for mortality rate
        double beta0;
        double beta1;
        double beta2;
        double ptrh1;     
                       
        Var _I1v;
        Ext _E1v;
        Ext _T1;
        Ext _Rh1;
       
                
    };

} // namespace malariaspread

DECLARE_DYNAMICS(malariaspread::I1v)

 