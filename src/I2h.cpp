/*
 * @file malaria/Si.cpp
 *
 * This file is part of VLE, a framework for multi-modeling, simulation
 * and analysis of complex dynamical systems
 * http://www.vle-project.org
 *
 * Copyright (c) 2011 INRA http://www.inra.fr
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* @@tagdepends: vle.extension.differential-equation @@endtagdepends
   @@tagdynamic@@ */

#include <vle/extension/DifferentialEquation.hpp>

#include <iostream>
#include <sstream>

namespace malariaspread  {

    namespace ved = vle::extension::differential_equation;

    class I2h :
        public ved::DifferentialEquation
    {
    public:
        I2h(const vle::devs::DynamicsInit& model,
           const vle::devs::InitEventList& events) :
            ved::DifferentialEquation(model,events)
        {
            delta_H2  = events.getDouble("delta_H2");; // = ???
            
	    alpha_2  = events.getDouble("alpha_2"); // = ???
           
            gamma_2  = events.getDouble("gamma_2"); // = ???
            
            d2_H2  = events.getDouble("d2_H2"); // = ???

                   
            _I2h = createVar("I2h");
            _E2h = createExt("E2h");           

            
        }

        virtual ~I2h()
        { }

        void compute(const vle::devs::Time& /* time */)
        {
            grad(_I2h) = delta_H2*_E2h() - alpha_2*_I2h() - gamma_2*_I2h() - d2_H2*_I2h();
        }

    private:
        
        // transition rate from E -> I for the patch 2 of the host population
        double delta_H2;
        // transition rate from I -> R for the patch 2 of the host population
        double alpha_2;
        //death rate due to the disease
        double gamma_2;
        // natural death rate of I in the host population for the patch 2
        double d2_H2;        
                       
        Var _I2h;
        Ext _E2h;
                
    };

} // namespace malariaspread

DECLARE_DYNAMICS(malariaspread::I2h)

 