/*
 * @file malaria/Si.cpp
 *
 * This file is part of VLE, a framework for multi-modeling, simulation
 * and analysis of complex dynamical systems
 * http://www.vle-project.org
 *
 * Copyright (c) 2011 INRA http://www.inra.fr
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* @@tagdepends: vle.extension.differential-equation @@endtagdepends
   @@tagdynamic@@ */

#include <vle/extension/DifferentialEquation.hpp>

#include <iostream>
#include <sstream>

namespace malariaspread  {

    namespace ved = vle::extension::differential_equation;

    class I2v :
        public ved::DifferentialEquation
    {
    public:
        I2v(const vle::devs::DynamicsInit& model,
           const vle::devs::InitEventList& events) :
            ved::DifferentialEquation(model,events)
        {
            
            
            delta_V2  = events.getDouble("delta_V2");; // = ???

            d2_V2  = events.getDouble("d2_V2"); // = ???
                  
                     
            _I2v = createVar("I2v");
            _E2v = createExt("E2v"); 
           
            _T2 = createExt("Tmin");
            _Rh2 = createExt("Hmax");
            
        }

        virtual ~I2v()
        { }

        void compute(const vle::devs::Time& /* time */)
        {
            beta0 = 0.00113*_Rh2()*_Rh2()-0.158*_Rh2()-6.61; 
            beta1 = -2.32*pow(10,-4)*_Rh2()*_Rh2()+0.0515*_Rh2()+1.06;
            beta2 = 4*pow(10,-6)*_Rh2()*_Rh2()-1.09*pow(10,-3)*_Rh2()-0.0255;          
            
            ptrh2 = -log(exp(-1/(beta0+beta1*_T2()+beta2*_T2()*_T2())));    // mortality rate of the vector at temperature t1 and relative humidity rh1
              
            //grad(_I2v) = delta_V2*_E2v() - ptrh2*_I2v() - d2_V2*_I2v();

           grad(_I2v) = delta_V2*_E2v() - ptrh2*_I2v();

        }

    private:
       
        // transition rate from E -> I of the vector population for the patch 2
        double delta_V2;
        // natural death rate of I in the vector population for the patch 2
        double d2_V2;     
        //coefficients for mortality rate
        double beta0;
        double beta1;
        double beta2;
        double ptrh2;   
                       
        Var _I2v;
        Ext _E2v;
        Ext _T2;
        Ext _Rh2;
                
    };

} // namespace malariaspread

DECLARE_DYNAMICS(malariaspread::I2v)

 