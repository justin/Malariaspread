/*
 * @file malaria/Si.cpp
 *
 * This file is part of VLE, a framework for multi-modeling, simulation
 * and analysis of complex dynamical systems
 * http://www.vle-project.org
 *
 * Copyright (c) 2011 INRA http://www.inra.fr
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* @@tagdepends: vle.extension.differential-equation @@endtagdepends
   @@tagdynamic@@ */

#include <vle/extension/DifferentialEquation.hpp>

#include <iostream>
#include <sstream>

namespace malariaspread  {

    namespace ved = vle::extension::differential_equation;

    class R1h :
        public ved::DifferentialEquation
    {
    public:
        R1h(const vle::devs::DynamicsInit& model,
           const vle::devs::InitEventList& events) :
            ved::DifferentialEquation(model,events)
        {
            
            
	    alpha_1  = events.getDouble("alpha_1"); // = ???

            d1_H1  = events.getDouble("d1_H1"); // = ???

            phi_r1  = events.getDouble("phi_r1"); // = ???
       
            phi_r2  = events.getDouble("phi_r2"); // = ???

            epsilon_1  = events.getDouble("epsilon_1"); // = ???
                    
                        
            _R1h = createVar("R1h");
            _I1h = createExt("I1h"); 
            _R2h= createExt("R2h");          

            
        }

        virtual ~R1h()
        { }

        void compute(const vle::devs::Time& /* time */)
        {
            grad(_R1h) = alpha_1*_I1h() + phi_r2*_R2h() - phi_r1*_R1h() - epsilon_1*_R1h() - d1_H1*_R1h();
        }

    private:

        // transition rate from i -> R for the patch i of the host population
        double alpha_1;
        // natural death rate of R in the host population for the patch i
        double d1_H1;        
        //transition rate from R -> S in the host population for the patch 2 
        double epsilon_1; 

        // Recovered rate of host population of the patch 1 that left to patch 2 (Go)
        double phi_r1;        
        // migration rate of Recovered (R) of host population of the patch 2 that come to patch 1 (Come)
        double phi_r2;  
                       
        Var _R1h;
        Ext _I1h;
        Ext _R2h;
                
    };

} // namespace malariaspread

DECLARE_DYNAMICS(malariaspread::R1h)

 