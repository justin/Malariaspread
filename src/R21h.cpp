/*
 * @file malaria/Si.cpp
 *
 * This file is part of VLE, a framework for multi-modeling, simulation
 * and analysis of complex dynamical systems
 * http://www.vle-project.org
 *
 * Copyright (c) 2011 INRA http://www.inra.fr
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* @@tagdepends: vle.extension.differential-equation @@endtagdepends
   @@tagdynamic@@ */

#include <vle/extension/DifferentialEquation.hpp>

#include <iostream>
#include <sstream>

namespace malariaspread  {

    namespace ved = vle::extension::differential_equation;

    class R21h :
        public ved::DifferentialEquation
    {
    public:
        R21h(const vle::devs::DynamicsInit& model,
           const vle::devs::InitEventList& events) :
            ved::DifferentialEquation(model,events)
        {
            
            
	    alpha_1  = events.getDouble("alpha_1"); // = ???

            d1_H1  = events.getDouble("d1_H1"); // = ???

            phi_r2  = events.getDouble("phi_r2"); // = ???
       
                           
                        
            _R21h = createVar("R21h");
            _I21h = createExt("I21h"); 
            _R2h= createExt("R2h");          

            
        }

        virtual ~R21h()
        { }

        void compute(const vle::devs::Time& /* time */)
        {
            grad(_R21h) = alpha_1*_I21h() + phi_r2*_R2h() - d1_H1*_R21h();
        }

    private:

        // transition rate from i -> R for the patch i of the host population
        double alpha_1;
        // natural death rate of R in the host population for the patch i
        double d1_H1;        

        // Recovered rate of host population of the patch 2 that left to patch 1 (Go)
        double phi_r2;        
        
                       
        Var _R21h;
        Ext _I21h;
        Ext _R2h;
                
    };

} // namespace malariaspread

DECLARE_DYNAMICS(malariaspread::R21h)

 