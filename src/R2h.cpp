/*
 * @file malaria/Si.cpp
 *
 * This file is part of VLE, a framework for multi-modeling, simulation
 * and analysis of complex dynamical systems
 * http://www.vle-project.org
 *
 * Copyright (c) 2011 INRA http://www.inra.fr
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* @@tagdepends: vle.extension.differential-equation @@endtagdepends
   @@tagdynamic@@ */

#include <vle/extension/DifferentialEquation.hpp>

#include <iostream>
#include <sstream>

namespace malariaspread  {

    namespace ved = vle::extension::differential_equation;

    class R2h :
        public ved::DifferentialEquation
    {
    public:
        R2h(const vle::devs::DynamicsInit& model,
           const vle::devs::InitEventList& events) :
            ved::DifferentialEquation(model,events)
        {
            
            
	    alpha_2  = events.getDouble("alpha_2"); // = ???

            d2_H2  = events.getDouble("d2_H2"); // = ???
  
            epsilon_2  = events.getDouble("epsilon_2"); // = ???

            phi_r2  = events.getDouble("phi_r2"); // = ???
       
            phi_r1  = events.getDouble("phi_r1"); // = ???
                    
                        
            _R2h = createVar("R2h");
            _I2h = createExt("I2h");
            _R1h= createExt("R1h");            

            
        }

        virtual ~R2h()
        { }

        void compute(const vle::devs::Time& /* time */)
        {
            grad(_R2h) = alpha_2*_I2h() + phi_r1*_R1h() - phi_r2*_R2h() - epsilon_2*_R2h() - d2_H2*_R2h();
        }

    private:

        // transition rate from I -> R for the patch 2 of the host population
        double alpha_2;
        // natural death rate of R in the host population for the patch 2
        double d2_H2;  
         //transition rate from R -> S in the host population for the patch 2 
        double epsilon_2;      

        // migration rate of Recovered of host population of the patch 2 that left to patch 1 (Go)
        double phi_r2;        
        // migration rate of Recovered (R) of host population of the patch 1 that come to patch 2 (Come)
        double phi_r1; 
                       

                       
        Var _R2h;
        Ext _I2h;
        Ext _R1h;
                
    };

} // namespace malariaspread

DECLARE_DYNAMICS(malariaspread::R2h)

 