/*
 * @file malaria/Si.cpp
 *
 * This file is part of VLE, a framework for multi-modeling, simulation
 * and analysis of complex dynamical systems
 * http://www.vle-project.org
 *
 * Copyright (c) 2011 INRA http://www.inra.fr
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* @@tagdepends: vle.extension.differential-equation @@endtagdepends
   @@tagdynamic@@ */

#include <vle/extension/DifferentialEquation.hpp>

#include <iostream>
#include <sstream>

namespace malariaspread  {

    namespace ved = vle::extension::differential_equation;

    class S12h :
        public ved::DifferentialEquation
    {
    public:
        S12h(const vle::devs::DynamicsInit& model,
           const vle::devs::InitEventList& events) :
            ved::DifferentialEquation(model,events)
        {
            
           // mu2_H2  = events.getDouble("mu2_H2");  

            k_2  = events.getDouble("k_2"); // = ???
            
	    beta_2  = events.getDouble("beta_2"); // = ???

            // d2_H2  = events.getDouble("d2_H2"); 

            // H_2  = events.getDouble("H_2"); 
          
             H_2  = events.getDouble("H_2"); 

            phi_s1  = events.getDouble("phi_s1"); // = ???
       
     
 
             epsilon_2  = events.getDouble("epsilon_2"); // = ???

    	                        

            _S12h = createVar("S12h");
            _I2v = createExt("I2v");
	   // _Pth2 = createExt("Pth2");
            _S1h= createExt("S1h");
            _R12h= createExt("R12h"); 

            /* _S2v= createExt("S2v");
             _E2v= createExt("E2v");*/
            
        }

        virtual ~S12h()
        { }

        void compute(const vle::devs::Time& /* time */)
        {
           // H_2 = grad(_S2h) + grad(_E2h) + grad(_I2h);   //current size of all compartments at time t
          
            grad(_S12h) = epsilon_2*_R12h() + phi_s1*_S1h() - k_2*beta_2*_S12h()*_I2v()/H_2 - d2_H2*_S12h();
        }

    private:
       
        //birth rate of host population of the patch i
       // double mu2_H2;

        //contact proportion between Susceptibles humans of the patch 1 and Infectious mosquitoes of the patch 1
        double beta_2;
        //average number of that contact per unit time for that patch i
        double k_2;

       
        // size of host population for the patch 1
        double H_2;

        // natural death rate of S in the host population for the patch i
        double d2_H2;

       // migration rate of Susceptible of host population of the patch 1 that left to patch 2 (Go)
        double phi_s1;        
          

       //transition rate from R -> S in the host population for the patch 2 
        double epsilon_2; 

        
        Var _S12h;
        Ext _I2v;
        //Ext _Pth2;
        Ext _S1h;
        Ext _R12h;

        //Ext _S2v;
        //Ext _E2v;
        
        
        
    };

} // namespace malariaspread

DECLARE_DYNAMICS(malariaspread::S12h)

 