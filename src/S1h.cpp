/*
 * @file malaria/Si.cpp
 *
 * This file is part of VLE, a framework for multi-modeling, simulation
 * and analysis of complex dynamical systems
 * http://www.vle-project.org
 *
 * Copyright (c) 2011 INRA http://www.inra.fr
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* @@tagdepends: vle.extension.differential-equation @@endtagdepends
   @@tagdynamic@@ */

#include <vle/extension/DifferentialEquation.hpp>

#include <iostream>
#include <sstream>

namespace malariaspread  {

    namespace ved = vle::extension::differential_equation;

    class S1h :
        public ved::DifferentialEquation
    {
    public:
        S1h(const vle::devs::DynamicsInit& model,
           const vle::devs::InitEventList& events) :
            ved::DifferentialEquation(model,events)
        {
            
            mu1_H1  = events.getDouble("mu1_H1");  // = ???

            //k_1  = events.getDouble("k_1"); // = ???
            
	    //beta_1  = events.getDouble("beta_1"); // = ???
 
            b_1  = events.getDouble("b_1"); // = ???

            d1_H1  = events.getDouble("d1_H1"); // = ???

            H_1  = events.getDouble("H_1"); // = ???

         
            phi_s1  = events.getDouble("phi_s1"); // = ???
       
            phi_s2  = events.getDouble("phi_s2"); // = ???
 
            epsilon_1  = events.getDouble("epsilon_1"); // = ???

    	                        

            _S1h = createVar("S1h");
            _I1v = createExt("I1v");
            _R1h = createExt("R1h");
	    //_Pth1 = createExt("Pth1");
            _S2h= createExt("S2h");

           /* _S1v= createExt("S1v");
              _E1v= createExt("E1v"); */
            
        }

        virtual ~S1h()
        { }

        void compute(const vle::devs::Time& /* time */)
        {
           // H_1 = grad(_S1h) + grad(_E1h) + grad(_I1h);   //current size of all compartments at time t  
      
                   
            grad(_S1h) = mu1_H1*H_1 + phi_s2*_S2h() - phi_s1*_S1h() + epsilon_1*_R1h() - b_1*_S1h()*_I1v()/H_1 - d1_H1*_S1h();
        }

    private:
       
        //birth rate of host population of the patch 1
        double mu1_H1;
        //contact proportion between Susceptibles humans of the patch 1 and Infectious mosquitoes of the patch 1
        //double beta_1;
        //average number of that contact per unit time for that patch 1
        //double k_1;
        //inoculation rate of patch 1
        double b_1;
       
        // size of host population for the patch 1
        double H_1;
        // natural death rate of S in the host population for the patch i
        double d1_H1;
        // migration rate of Susceptibles of host population of the patch 1 that left to patch 2 (Go)
        double phi_s1;        
         // migration rate of Susceptible of host population of the patch 2 that come to patch 1 (Come)
        double phi_s2;  
       //transition rate from R -> S in the host population for the patch 2 
        double epsilon_1;
        
        Var _S1h;
        Ext _I1v;
        Ext _R1h;
        //Ext _Pth1;
        Ext _S2h;

     /* Ext _S1v;
        Ext _E1v; */
        
        
    };

} // namespace malariaspread

DECLARE_DYNAMICS(malariaspread::S1h)

 