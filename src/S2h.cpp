/*
 * @file malaria/Si.cpp
 *
 * This file is part of VLE, a framework for multi-modeling, simulation
 * and analysis of complex dynamical systems
 * http://www.vle-project.org
 *
 * Copyright (c) 2011 INRA http://www.inra.fr
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* @@tagdepends: vle.extension.differential-equation @@endtagdepends
   @@tagdynamic@@ */

#include <vle/extension/DifferentialEquation.hpp>

#include <iostream>
#include <sstream>

namespace malariaspread  {

    namespace ved = vle::extension::differential_equation;

    class S2h :
        public ved::DifferentialEquation
    {
    public:
        S2h(const vle::devs::DynamicsInit& model,
           const vle::devs::InitEventList& events) :
            ved::DifferentialEquation(model,events)
        {
            
            mu2_H2  = events.getDouble("mu2_H2");  // = ???

            //k_2  = events.getDouble("k_2"); // = ???
            
	    //beta_2  = events.getDouble("beta_2"); // = ???

 	    b_2  = events.getDouble("b_2"); // = ???

            d2_H2  = events.getDouble("d2_H2"); // = ???

            H_2  = events.getDouble("H_2"); // = ???

            epsilon_2  = events.getDouble("epsilon_2"); // = ???

            phi_s2  = events.getDouble("phi_s2"); // = ???
       
            phi_s1  = events.getDouble("phi_s1"); // = ???
    	                        

            _S2h = createVar("S2h");
            _I2v = createExt("I2v");
            _R2h = createExt("R2h");
	    //_Pth2 = createExt("Pth2");
            _S1h= createExt("S1h");
            
           /* _S2v= createExt("S2v");
              _E2v= createExt("E2v"); */
            
        }

        virtual ~S2h()
        { }

        void compute(const vle::devs::Time& /* time */)
        {
           // V_2 = grad(_S2v) + grad(_E2v) + grad(_I2v);   //current size of all compartments at time t  
        
 
            grad(_S2h) = mu2_H2*H_2 + phi_s1*_S1h() - phi_s2*_S2h() + epsilon_2*_R2h() - b_2*_S2h()*_I2v()/H_2 - d2_H2*_S2h();
        }

    private:
       
        //birth rate of host population of the patch 2
        double mu2_H2;
        //contact proportion between Susceptibles humans of the patch 2 and Infectious mosquitoes of the patch 2
        //double beta_2;
        //average number of that contact per unit time for that patch 2
        //double k_2;
        //inoculation rate of patch 2
        double b_2;
        // size of host population
        double H_2;

        // natural death rate of S in the host population for the patch 2
        double d2_H2;
        //transition rate from R -> S in the host population for the patch 2 
        double epsilon_2;

         // migration rate of Susceptible of host population of the patch 2 that left to patch 1 (Go)
        double phi_s2;        
       // migration rate of Susceptible of host population of the patch 1 that come to patch 2 (Come)
        double phi_s1;   
            
        
        Var _S2h;
        Ext _I2v;
        Ext _R2h;
        //Ext _Pth2;
        Ext _S1h;

     /* Ext _S2v;
        Ext _E2v; */
        
        
    };

} // namespace malariaspread

DECLARE_DYNAMICS(malariaspread::S2h)

 