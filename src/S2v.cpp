/*
 * @file malaria/Si.cpp
 *
 * This file is part of VLE, a framework for multi-modeling, simulation
 * and analysis of complex dynamical systems
 * http://www.vle-project.org
 *
 * Copyright (c) 2011 INRA http://www.inra.fr
 *
 * See the AUTHORS or Authors.txt file for copyright owners and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* @@tagdepends: vle.extension.differential-equation @@endtagdepends
   @@tagdynamic@@ */

#include <vle/extension/DifferentialEquation.hpp>

#include <iostream>
#include <sstream>

namespace malariaspread  {

    namespace ved = vle::extension::differential_equation;

    class S2v :
        public ved::DifferentialEquation
    {
    public:
        S2v(const vle::devs::DynamicsInit& model,
           const vle::devs::InitEventList& events) :
            ved::DifferentialEquation(model,events)
        {
            mu2_V2  = events.getDouble("mu2_V2"); // = ???

            f_2  = events.getDouble("f_2");; // = ???

	    omega_2  = events.getDouble("omega_2"); // = ???

            d2_V2  = events.getDouble("d2_V2"); // = ???

            V_2  = events.getDouble("V_2"); // = ???

            H_2  = events.getDouble("H_2"); // = ???
                       
            _S2v = createVar("S2v");
            _I2h = createExt("I2h");
          
            _T2 = createExt("Tmin");
            _Rh2 = createExt("Hmax");
            
        }

        virtual ~S2v()
        { }

        void compute(const vle::devs::Time& /* time */)
        {
            beta0 = 0.00113*_Rh2()*_Rh2()-0.158*_Rh2()-6.61; 
            beta1 = -2.32*pow(10,-4)*_Rh2()*_Rh2()+0.0515*_Rh2()+1.06;
            beta2 = 4*pow(10,-6)*_Rh2()*_Rh2()-1.09*pow(10,-3)*_Rh2()-0.0255;          
            
            ptrh2 = -log(exp(-1/(beta0+beta1*_T2()+beta2*_T2()*_T2())));    // mortality rate of the vector at temperature t1 and relative humidity rh1
              
         //   grad(_S2v) = mu2_V2 *V_2 - f_2*omega_2*_S2v()*_I2h()/H_2 - ptrh2*_S2v() - d2_V2*_S2v();
           grad(_S2v) = mu2_V2 *V_2 - f_2*omega_2*_S2v()*_I2h()/V_2 - ptrh2*_S2v();
        }

    private:

        //birth rate of vector population of the patch 2
        double mu2_V2;
        //contact proportion between Susceptibles mosquitoes of the patch 2 and Infectious humans of the patch 2
        double omega_2;
        //average number of that contact per unit time for that patch 2
        double f_2;
      
        // size of vector population for the patch 2
        double V_2;

        double H_2;

        // natural death rate of S in the vector population for the patch 2
        double d2_V2;
        //coefficients for mortality rate
        double beta0;
        double beta1;
        double beta2;
        double ptrh2;
        
        Var _S2v;
        Ext _I2h;
        Ext _T2;
        Ext _Rh2;
        
    };

} // namespace malariaspread

DECLARE_DYNAMICS(malariaspread::S2v)

 